{
  inputs = {
    nerdfonts = {
      url = "github:ryanoasis/nerd-fonts";
      flake = false;
    };
  };

  outputs = { self, nerdfonts, nixpkgs }: let
    forAllSystems = nixpkgs.lib.genAttrs [ "x86_64-linux" ];
  in {
    # Make it buildable with `nix build`
    packages = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
      python = pkgs.python3.withPackages (pypkgs: with pypkgs; [
        fontforge
        configparser
      ]);
    in {
      nix-nerd-patcher = pkgs.writeShellScriptBin "nix-nerd-patcher" ''
        ${python}/bin/python "${nerdfonts}/font-patcher" "$@"
      '';
    });
    defaultPackage = forAllSystems (system: self.packages."${system}".nix-nerd-patcher);

    # Make it runnable with `nix run`
    apps = forAllSystems (system: {
      nix-nerd-patcher = {
        type    = "app";
        program = "${self.packages."${system}".nix-nerd-patcher}/bin/nix-nerd-patcher";
      };
    });
    defaultApp = forAllSystems (system: self.apps."${system}".nix-nerd-patcher);
  };
}
