# [nerd-fonts](https://github.com/ryanoasis/nerd-fonts)' font patcher, but [![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

A friend recently had issues with getting this patcher to work, and while he
solved it on his own I thought this would be a good time to show-off how nice
Nix is. Also, I'm bored, I'm allowed to do fun things even if they are
pointless, so bugger off pal.

Note: It should be obvious, but the LICENSE applies to this repo and not nerd
font or fontforge or whatever. Just 'cause they're packaged here doesn't mean
they're following this repo's license.

## Usage

Firstly, you must of course [Get.](https://nixos.org/download.html) [Nix.](https://nixos.wiki/wiki/Nix_Installation_Guide)

Then, with [Nix flakes](https://nixos.wiki/wiki/Flakes#Installing_flakes):

``` sh
nix run gitlab:jD91mZM2/nix-nerd-patcher [...args]
```

or if you don't have the experimental Nix flakes (lol noob), you can use the
legacy interface

``` sh
nix-build https://gitlab.com/jD91mZM2/nix-nerd-patcher/-/archive/master.tar.gz
result/bin/nix-nerd-patcher [...args]
```
